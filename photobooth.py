#!/usr/bin/env python3

from fire import Fire
import tempfile
import os
import subprocess as sp
import shlex
import random

import numpy as np
import cv2
import screeninfo
from flashcam import usbcheck

import socket


def client_buzz():
    print("i... sending buzz")
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_socket.settimeout(2)  # Set timeout to 2 seconds
    try:
        client_socket.connect(('192.168.0.96', 12345))  # Replace with Bob's address and port
        client_socket.send("buzz".encode('utf-8'))
    except:
        print("X... fail")
    client_socket.close()

def client_cheer():
    print("i... sending cheer")
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_socket.settimeout(2)  # Set timeout to 2 seconds
    try:
        client_socket.connect(('192.168.0.96', 12345))  # Replace with Bob's address and port
        client_socket.send("cheer".encode('utf-8'))
    except:
        print("X... fail")
    client_socket.close()



def help(image):
    # Define text details
    font = cv2.FONT_HERSHEY_SIMPLEX
    font_scale = 1
    font_color = (0, 255, 0)  # White color
    line_type = 2
    text_position = (10, image.shape[0] - 10)  # Bottom left corner

    # Add text
    cv2.putText(image, 'q: quit', text_position, font, font_scale, font_color, line_type)
    cv2.putText(image, 'b: buzzer', (10, text_position[1] - 30), font, font_scale, font_color, line_type)
    cv2.putText(image, 'c: cheer', (10, text_position[1] - 60), font, font_scale, font_color, line_type)
    cv2.putText(image, 'space:  print', (10, text_position[1] - 90), font, font_scale, font_color, line_type)
    cv2.putText(image, 'n: NO print', (10, text_position[1] - 120), font, font_scale, font_color, line_type)


def capture( videonum ):
    """
    give video numnber - open cam, show window, press space to return image path
    """
    # initialize the camera
    #cv2.namedWindow("camtest",  cv2.WINDOW_NORMAL )
    cv2.namedWindow("camtest",  cv2.WINDOW_AUTOSIZE )
    cv2.setWindowProperty("camtest", cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN )

#<<<<<<< HEAD
#    cam = cv2.VideoCapture(2)   # 0 -> index of camera
#=======
    cam = cv2.VideoCapture( videonum )   # 0 -> index of camera
#>>>>>>> 2e18c0c64a9f2b8ee8f35b13a31cfbde930b13ad
    cam.set(3,640)
    cam.set(4,480)

    screen_id = 0
    is_color = False

    # get the size of the screen
    print( screeninfo.get_monitors() )
    screen = screeninfo.get_monitors()[screen_id]
    width, height = screen.width, screen.height
    window_name = "camtest"

    while True:
        s, img = cam.read()
        img = cv2.rotate(img, cv2.ROTATE_90_CLOCKWISE)
        img2 = img.copy()
        help(img)
        if s:    # frame captured without any errors
            # ========================  move to the center =============
            h,w,d = img.shape
            cv2.moveWindow(window_name, int(screen.width/2 -w/2), int(screen.height/2 -h/2) )

            cv2.imshow(window_name,img)
            key = cv2.waitKey(90)
            keypress = key
            if key == ord("q"):
                return None
            if key == ord("n"):
                keypress = 'n'
            if key == ord("b"):
                client_buzz()
            if key == ord("c"):
                client_cheer()
            if key == ord(" ") or key == ord("n"):
                cv2.destroyWindow("camtest")
                cv2.destroyAllWindows()
                OUTPUT = get_tmp()
                cv2.imwrite(OUTPUT,img2) #save image
                return OUTPUT, keypress
    return None, keypress



def get_random_labels():
    months = ["ledna","února", "března", "dubna", "května", "června", "července", "srpna", "září", "října", "listopadu", "prosince" ]
    ftips = [
        ["WANTED!", "Dead, Alive or in Superposition"],
        ["Ústav jaderné fyziky AVČR", f"Nobelova cena za chemii r.20{random.randint(33,55)}"],
        ["Ústav jaderné fyziky AVČR", f"Nobelova cena za fyziku r.20{random.randint(33,55)}"],
        ["Ústav jaderné fyziky AVČR", f"Pracovník měsíce ... {random.choice(months)} "],
        ["Ústav jaderné fyziky AVČR", f"Pracovník měsíce ... {random.choice(months)} "],
        ["Ústav jaderné fyziky AVČR", "Ztratil se fyzik, volejte 158"],
        ["Ústav jaderné fyziky AVČR", "Největší jedlík v ústavu"],
        ["ÚJF AVČR", "Úplně Jiná Fyzika"],
        ["Ústav jaderné fyziky AVČR", "Akademik roku"],
        ["Ústav jaderné fyziky AVČR", "Pozor, radioaktivní kolega"],
        ["Ústav jaderné fyziky AVČR", f"Nejlepší student r.20{random.randint(30,40)}"],
        ["Ústav jaderné fyziky AVČR", f"Nejlepší student r.20{random.randint(30,40)}"]
        ]
    rand_el = random.choice(ftips)
    return rand_el


def get_tmp():
    """
    the printer understands to PNG
    """
    suffix = '.png'
    tmp_dir = '/tmp'
    temp_file = tempfile.NamedTemporaryFile(suffix=suffix, dir=tmp_dir, delete=False)
    temp_filename = temp_file.name
    temp_file.close()
    return temp_filename


def runme(CMDi):
    """
    run with the help of safe shlex
    """
    print("_"*(70-len(CMDi)), CMDi)
    CMD = shlex.split(CMDi)# .split()
    res=sp.check_output( CMD ).decode("utf8")
    print("i... RESULT:", res)
    print("#"*70)
    return res



def get_width(IMG):
  width=f"identify -format %w {IMG}"
  res = runme(width).strip()
  res = int(res)
  #print(f"i... image width=/{res}/")
  return res


def get_height(IMG):
  height = f"identify -format %h {IMG}"
  res = runme(height).strip()
  res = int(res)
  #print(f"i... image height=/{res}/")
  return res



def guess_points(IMG):
    """
    466 x 624 image has 32 points.....
    """
    WIDTH = get_width(IMG)
    HEIGHT = get_height(IMG)
    #linear:
    res = 32 * (WIDTH/466)
    return res


# ----------------------------------------------------------------------------

def annotate_north(IMG, TEXT, center = False):
    """
    put some text NORTH -
    """
    # position
    WIDTH = get_width(IMG)
    HEIGHT = get_height(IMG)
    #print("i... HEIGHT==",HEIGHT)
    OUTPUT = get_tmp()

    POINTS = guess_points(IMG)

    RAT = 1
    #print(len(TEXT))
    if len(TEXT)>32:
        RAT = 32/len(TEXT)
    POINTS = round(RAT*POINTS)
    GRAVITY = "west"
    if center: GRAVITY = "center"
    # 30 len 29
    #
    #CMD = f"montage -pointsize {POINTS} -label '{TEXT}'   {IMG} -geometry +0+0 -background white {OUTPUT}"
    CMD = f"convert -pointsize {POINTS} xc:none -gravity {GRAVITY}  -stroke black -strokewidth 2 -annotate 0 '{TEXT}' -background none -shadow 100x3+0+0 +repage -resize {WIDTH}x{POINTS}! -stroke none -fill white  -annotate 0 '{TEXT}' {IMG}  +swap -gravity north -geometry +0+{POINTS} -composite  {OUTPUT}"
    #convert -pointsize 100 label:"TEXT TO FIT" -trim +repage -resize 200x50! x.png

    runme(CMD)
    return OUTPUT



def annotate_south(IMG, TEXT):
    """
    put some text  32 points seems nice, MONTAGE
    """
    # position
    WIDTH = get_width(IMG)
    #HEIGHT = int(WIDTH/len(TEXT))
    OUTPUT = get_tmp()
    # CMD = f"convert -size 100x14 xc:none -gravity center  -stroke black -strokewidth 2 -annotate 0 '{TEXT}' -background none -shadow 100x3+0+0 +repage -stroke none -fill white  -annotate 0 '{TEXT}' {IMG}  +swap -gravity south -geometry +0-3 -composite  {OUTPUT}"
    # CMD = f"convert -size {WIDTH}x{HEIGHT} xc:none -gravity center  -stroke black -strokewidth 2 -annotate 0 '{TEXT}' -background none -shadow 100x3+0+0 +repage -stroke none -fill white  -annotate 0 '{TEXT}' {IMG}  +swap -gravity south -geometry +0-3 -composite  {OUTPUT}"

    # CMD = f"convert -pointsize 30 xc:none -gravity center  -stroke black -strokewidth 2 -annotate 0 '{TEXT}' -background none -shadow 100x3+0+0 +repage -resize {WIDTH}x50! -stroke none -fill white  -annotate 0 '{TEXT}' {IMG}  +swap -gravity south -geometry +0-3 -composite  {OUTPUT}"

    # CMD =f"convert -background '#0008' -fill white -gravity center -size ${WIDTH}x30   -caption '{TEXT}' {IMG} +swap -gravity south -composite  {OUTPUT}"

    POINTS = guess_points(IMG) #32
    RAT = 1
    #print(len(TEXT))
    if len(TEXT)>30:
        RAT = 30/len(TEXT)
    POINTS = round(RAT*POINTS)
    # 30 len 29
    #
    CMD = f"montage -pointsize {POINTS} -label '{TEXT}'  {IMG} -geometry +0+0 -background white {OUTPUT}"
    #convert -pointsize 100 label:"TEXT TO FIT" -trim +repage -resize 200x50! x.png

    runme(CMD)
    return OUTPUT




def dither(IMG, percent=50):
        #width = 707-10
    OUTPUT = get_tmp()
    #CMD="-auto-level  -scale "+str(width)+"x   -monochrome -dither FloydSteinberg  -remap pattern:gray50  "+OUTPUT
    CMD=f"convert {IMG} -auto-level   -monochrome -dither FloydSteinberg  -remap pattern:gray{percent}  {OUTPUT}"
    runme(CMD)
    return OUTPUT




def rotate_img(IMG):
    OUTPUT = get_tmp()
    CMD = f"convert {IMG} -rotate 90 {OUTPUT}"
    runme(CMD)
    return OUTPUT



def resize_img(IMG, factor = 0.5):
    OUTPUT = get_tmp()
    CMD = f"    convert {IMG}    -resize {round(factor*100)}%   {OUTPUT}"
    runme(CMD)
    return OUTPUT


def rescale_img(IMG, maxw = 714):
    """
    62x   brother  eats 714 px width, then it can crash
    """
    OUTPUT = get_tmp()
    CMD = f"    convert {IMG}    -resize x{maxw}   {OUTPUT}"
    runme(CMD)
    return OUTPUT



def annotate_img(IMG, north=" ", south=" "):
    WIDTH = get_width(IMG)
    HEIGHT = get_height(IMG)
    #print("i... HEIGHT==",HEIGHT)
    OUTPUTN = get_tmp()
    OUTPUTS = get_tmp()
    OUTPUT = get_tmp()

    POINTS = guess_points(IMG)

    IMN=""
    IMS=""
    if len(north.strip())>0:
        #CMD = f"convert -background white -fill black -gravity center -size {WIDTH}x label:{north} NORTH.png"
        CMD = f"convert -background white -fill black -gravity center -pointsize {POINTS} -size {WIDTH}x{POINTS}  label:'{north}' {OUTPUTN}"
        runme(CMD)
        IMN=OUTPUTN#"NORTH.png"
    if len(south.strip())>0:
        #CMD = f"convert -background white -fill black -gravity center -size {WIDTH}x label:{south} SOUTH.png"
        CMD = f"convert -background white -fill black -gravity center -pointsize {POINTS} -size {WIDTH}x{POINTS}  label:'{south}' {OUTPUTS}"
        runme(CMD)
        IMS=OUTPUTS#"SOUTH.png"
    CMD = f"montage -geometry +0+0 -tile 1x {IMN} {IMG} {IMS} {OUTPUT}"
    runme(CMD)
    return OUTPUT



# ==============================================================================================
# ==============================================================================================
# ==============================================================================================
def main( imgname=None , nlabel=None, slabel=None , printme=False, format="62", rotate = False, xsize = None, usbport =  "1a.0-usb-0:1.2:1.0"):
    """
"14.0-usb-0:1:1.0"
    Print image on QL700 printer

    Args:
      imgname: filename to print
      nlabel:  north label
      label:  bottom-south label

    Returns:
       Bool: status code
    """
    while True:

        print("i... Call ql570 to print on brother QL700 directly. Be in lp group")
        print("i...   USE  62   - DK-22205   for printing")
        print("i... rotate is necessary to print correctly???")
        print()
        img_work = imgname
        myformat = str(format)


        # REDEFINE
        if imgname is None:
            vidnum = usbcheck.recommend_video( usbport )
            print("i... VIDEONUMBER = ", vidnum)
            img_work, keypress = capture( vidnum[0] )
            print(f"========================={keypress}================")
        if img_work is None: # returns on 'q'
            break
        if keypress == ord('n'):
            print("i... n returned, NOT printing")
            printme = False
        elif keypress == ord(' '):
            printme = True
            print("i... ' ' returned, printing", printme, keypress)



        #img_work = test_img(img_work)

        if xsize is not None:
            img_work = resize_img(img_work, factor=xsize)



        # ==================================================== ANNOTATE NOR
        #img_work = annotate(img_work,"Zaslouzily pracovnik UJF AVCR")
        #if nlabel is not None:
        #    img_work = annotate_north(img_work,nlabel, center=True)



        # ==================================================== ANNOTATE
        #img_work = annotate(img_work,"Zaslouzily pracovnik UJF AVCR")
        #if label is not None:
        #    img_work = annotate_south(img_work,label)
        #else:
        #    img_work = annotate_south(img_work,"Zasloužilý generální tajemník UJF AVČR")


        if nlabel is None or slabel is None:
            nlabel,slabel = get_random_labels()
        img_work = annotate_img(img_work, nlabel, slabel)


        #
        # for printing....
        #
        if rotate:
            img_work = rotate_img(img_work)




        # ===================================================== DITHER
        img_work = rescale_img(img_work, maxw = 714) #
        img_work = dither(img_work, percent = 30)



        #  myformat 62 ..... FANTASTIC......
        #  29x90 ... works, but....
        #
        #
        if printme:
            CMD = f"./ql570 /dev/usb/lp0 {myformat} {img_work}"
            #CMD = f"geeqie {OUTPUT}"
            runme(CMD)
        else:
            #CMD = f"./ql570 /dev/usb/lp0 62 {OUTPUT}"
            CMD = f"geeqie {img_work}"
            runme(CMD)
    # =======================================================

    return True




if __name__=="__main__":
    Fire(main)
