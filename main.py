#!/usr/bin/env python3

from fire import Fire
from  ql_utils import runme, annotate_img, \
    rotate_img, rescale_img, dither, \
    monochrom, make_triple, \
    check_lpx

import tempfile
import os
import subprocess as sp
import shlex
import random

import numpy as np
import cv2
import screeninfo
from flashcam import usbcheck

import socket
import shutil
from console import fg,bg

import qrcode

def main( img_or_text ,
          command = "v",
          imgname  = None,
          paper = 62,
          xsize = None,
          ysize = None, # by default 1
          FONTSIZE = None,
          rotate = None,
          qr=False
         ):
    """
    Print a lablel with ql700 printer. \n
      for multiline use Alt-Enter
    Args:
        img_or_text: Give here either a text /max.two lines=north/south/  OR an image to process
        command:  possibilities:  -q-uit  -p-rint  -v-iew - default 'v'
        imgname:  if output to a specific filename is needed, else automatic /tmp ???
        paper: For now it works with 62, wide label
        xsize: possibility to resize to certain x size, do not use
        ysize: force the height of the label (width == 714 @62case), internal default is 1
        FONTSIZE: avoid automatic guess - try 100
        rotate: for webcam, rotation may be needed.
        qrcode: create qr code from "id:12345;owner:Pepa z Depa;aaa:etc;name:This is displayed and id is"

./main.py /tmp/ql700_showa.png ... just prints the image
./main.py "asd:123;id:12315;name:Jakesovy boty"  -q  ... qr code with id label
./main.py text ... just text
    """
    print("#"*70)

    print("i... I Call ql570 to print on brother QL700 directly. Be in lp group")
    print("i...   USE  62   - DK-22205   for printing")
    print("i...   ")
    print()

    IMG_SHOW_PATH = "/tmp/ql700_show.png"
    IMG_VIEWER = "geeqie -t"
    IMG_MODE = None # dither for image
    MAIN_WIDTH_PARAMETER = 714
    height,width = 480,640
    height,width = 480,240
    height,width = 1,714 # a trick for text
    img_work = imgname # None
    keypress = None
    printme = None
    myformat = str(paper)
    slabel = None # no south label for the monent


    qft = str(img_or_text)
    if os.path.exists(qft) and (qft.find(".jpg")>0 or qft.find(".png")>0):
        print("i... IMAGE filename detected, ditching sentence")
        sentence = None
        IMG_MODE = True
        # doesnt work imagename = qft
        img_work = qft
        print("D... img_work", img_work)
    else:
        print("i... sentence detected, not image")
        sentence = qft
        IMG_MODE = False

    print("D... img_work", img_work)

    # REDEFINE  ???
    if img_work is None:
        imgname = "/tmp/blank.jpg"
        if  ysize is not None:
            height = ysize
        blank_image = np.zeros((height,width,3), np.uint8)
        blank_image[:,:] = (55,155,255)
        cv2.imwrite( imgname , blank_image)
        img_work = imgname

    print(f"D... img_work === {img_work}")

    if img_work is None: # returns on 'q'
        return

    if sentence is None:
        nlabel = input("> Sentence : ")
    else:
        nlabel = sentence

    #print("i... ",  sentence.split("\n"))
    if len(nlabel.split("\n"))>1:
        nor,sou = nlabel.split("\n")[0] , nlabel.split("\n")[1]
        nlabel = nor
        slabel = sou
        print("i... SPLIT IS THERE: ",  nlabel, "x", slabel)
    else:
        print("D... NO SPLIT NO MULTILINE")


    if qr:
        # nlabel seems DONE
        qrtag = f"{nlabel};{slabel}"
        print(f"i... QRTAG (total string North+South) {fg.cyan}{qrtag}{fg.default}")
        # I will join Name: asoidj ; owner: OKjd ; location: asd o; ID: 8998244;
        qr = qrcode.make( qrtag)
        qrs = qrtag.split(";")
        for q in qrs:
            nam, *val= q.split(":")
            if len(val) == 0:continue
            if nam.strip().find( "id") == 0:
                nlabel = val[0]
            if nam.strip().find( "name") == 0:
                slabel = val[0]
            print(f"i... qr tag -  name: {nam} val: {val[0]},  LABELS SN: {nlabel}, {slabel}")

        qr.save("/tmp/qrcode.png")
        qft = make_triple("/tmp/qrcode.png")
        IMG_MODE = True
        #qft = "/tmp/qrcode.png"
        img_work = qft



    if command is None:
        keypress = input("> Give me a command: q/p/v (quit,print,view) :")
        if keypress == "":
            keypress = "q"
        keypress = ord(keypress)
        print(keypress)
    else:
        keypress = command
        keypress = ord(keypress)


    if keypress == ord('n'):
        print(f"i... n returned, {fg.red} NOT printing {fg.default}")
        printme = False
    elif keypress == ord('p'):
        printme = True
        print(f"i... SPACE or p ...{fg.green} printing {fg.default}", printme, keypress)
    elif keypress == ord('q'):
        print(f"i... q pressed - {bg.red}{fg.white}  QUIT   {bg.default}{fg.default}", printme, keypress)
        return
    elif keypress == ord('v'):
        print(f"i... v pressed - {bg.green}{fg.white}  viewing   {bg.default}{fg.default}", printme, keypress)
    else:
        print(f"i... unknown commad - {bg.red}{fg.white}  QUITing   {bg.default}{fg.default}", printme, keypress)
        return
        printme = False
        print("i... default printme = False")
    # printme = False

    #img_work = test_img(img_work)
# ===========================================================================================
#       commands are clear .... DO THINGS NOW
# ===========================================================================================

    # - IF SIZE GIVEN :::: re size first
    if xsize is not None: # some resize given in cmdline
        print(f"D... resizing to {xsize}")
        img_work = resize_img(img_work, factor=xsize)

    if nlabel is None and slabel is None: # BOTH No north,south -- randoms --
        pass# nlabel,slabel = get_random_labels()
    else:
        # Annotate: in case of no image : TRICK, annotate one line
        print("i... annotating:", img_work)
        img_work = annotate_img(img_work, nlabel, slabel, points = FONTSIZE)
        print("i.. annotated:")



    # =====================================================================
    # for printing....
    #
    if rotate:
        print("D... ROTATING because of commandline")
        img_work = rotate_img(img_work)
    if rotate is None and printme:
        print("D... ROTATING for printing")
        img_work = rotate_img(img_work)


    # ===========================================================================
    #
    #  THA LAST THING TO DO  ====== MONOCHOMIZE
    #
    # ===================================================== DITHER
    img_work = rescale_img(img_work, maxw = MAIN_WIDTH_PARAMETER) #
    if IMG_MODE:
        img_work = dither(img_work, percent = 30)
    else:
        img_work = monochrom(img_work)



    #  myformat 62 ..... FANTASTIC......
    #  29x90 ... works, but....
    #
    #
    if printme:
        res = runme("groups").split()
        print("i... groups:",res)
        if not "lp" in res:
            print("X.. you arre not in lp group")
            return
        lpx = check_lpx()
        CMD = f"./ql570 {lpx} {myformat} {img_work}"
        #CMD = f"geeqie {OUTPUT}"
        runme(CMD)
    else:
        #CMD = f"./ql570 /dev/usb/lp0 62 {OUTPUT}"
        shutil.copy(img_work,  IMG_SHOW_PATH)
        # check if geeqie is runnung
        CMD = "ps -ef"
        allps = runme(CMD,silent= True).split("\n")
        allps = [x for x in allps if x.find(IMG_VIEWER.split()[0])>0 ]
        allps = [x for x in allps if x.find(IMG_SHOW_PATH)>0 ]
        print(fg.green, allps, fg.default)
        # if no geeqie with my image, launch one
        if len(allps)<1:
            CMD = f"{IMG_VIEWER} {IMG_SHOW_PATH}"
            #sp.Popen([IMG_VIEWER,   IMG_SHOW_PATH])
            sp.Popen(CMD.split())
        #runme(CMD)
# =======================================================

if __name__=="__main__":
    Fire(main)
