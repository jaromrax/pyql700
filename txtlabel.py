#!/usr/bin/env python3

from fire import Fire
import tempfile
import os
import subprocess as sp
import shlex
import random

import cv2

import numpy as np
import cv2
import screeninfo
from flashcam import usbcheck

import socket


def client_buzz():
    print("i... sending buzz")
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_socket.settimeout(2)  # Set timeout to 2 seconds
    try:
        client_socket.connect(('192.168.0.96', 12345))  # Replace with Bob's address and port
        client_socket.send("buzz".encode('utf-8'))
    except:
        print("X... fail")
    client_socket.close()

def client_cheer():
    print("i... sending cheer")
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_socket.settimeout(2)  # Set timeout to 2 seconds
    try:
        client_socket.connect(('192.168.0.96', 12345))  # Replace with Bob's address and port
        client_socket.send("cheer".encode('utf-8'))
    except:
        print("X... fail")
    client_socket.close()



def help(image):
    return None


def get_random_labels():
    ftips = [
        ["OJR UJF AVČR", f"Inventarni cislo {random.randint(1000,9999)}"]
        ]
    rand_el = random.choice(ftips)
    return rand_el


def get_tmp():
    """
    the printer understands to PNG
    """
    suffix = '.png'
    tmp_dir = '/tmp'
    temp_file = tempfile.NamedTemporaryFile(suffix=suffix, dir=tmp_dir, delete=False)
    temp_filename = temp_file.name
    temp_file.close()
    return temp_filename


def runme(CMDi, silent = False):
    """
    run with the help of safe shlex
    """
    print("_"*(70-len(CMDi)), CMDi)
    CMD = shlex.split(CMDi)# .split()
    res=sp.check_output( CMD ).decode("utf8")
    if not silent:
        print("i... RESULT:", res)
        #print("#"*70)
    return res



def get_width(IMG):
  width=f"identify -format %w {IMG}"
  res = runme(width, silent = True).strip()
  res = int(res)
  #print(f"i... image width=/{res}/")
  return res


def get_height(IMG):
  height = f"identify -format %h {IMG}"
  res = runme(height, silent = True).strip()
  res = int(res)
  #print(f"i... image height=/{res}/")
  return res



def guess_points(IMG):
    """
    466 x 624 image has 32 points.....
    """
    WIDTH = get_width(IMG)
    HEIGHT = get_height(IMG)
    #linear:
    res = 32 * (WIDTH/466)
    return res


# ----------------------------------------------------------------------------

def annotate_north(IMG, TEXT, center = False):
    """
    put some text NORTH -
    """
    # position
    WIDTH = get_width(IMG)
    HEIGHT = get_height(IMG)
    #print("i... HEIGHT==",HEIGHT)
    OUTPUT = get_tmp()

    POINTS = guess_points(IMG)

    RAT = 1
    #print(len(TEXT))
    if len(TEXT)>32:
        RAT = 32/len(TEXT)
    POINTS = round(RAT*POINTS)
    GRAVITY = "west"
    if center: GRAVITY = "center"
    # 30 len 29
    #
    #CMD = f"montage -pointsize {POINTS} -label '{TEXT}'   {IMG} -geometry +0+0 -background white {OUTPUT}"
    CMD = f"convert -pointsize {POINTS} xc:none -gravity {GRAVITY}  -stroke black -strokewidth 2 -annotate 0 '{TEXT}' -background none -shadow 100x3+0+0 +repage -resize {WIDTH}x{POINTS}! -stroke none -fill white  -annotate 0 '{TEXT}' {IMG}  +swap -gravity north -geometry +0+{POINTS} -composite  {OUTPUT}"
    #convert -pointsize 100 label:"TEXT TO FIT" -trim +repage -resize 200x50! x.png

    runme(CMD)
    return OUTPUT



def annotate_south(IMG, TEXT):
    """
    put some text  32 points seems nice, MONTAGE
    """
    # position
    WIDTH = get_width(IMG)
    #HEIGHT = int(WIDTH/len(TEXT))
    OUTPUT = get_tmp()
    # CMD = f"convert -size 100x14 xc:none -gravity center  -stroke black -strokewidth 2 -annotate 0 '{TEXT}' -background none -shadow 100x3+0+0 +repage -stroke none -fill white  -annotate 0 '{TEXT}' {IMG}  +swap -gravity south -geometry +0-3 -composite  {OUTPUT}"
    # CMD = f"convert -size {WIDTH}x{HEIGHT} xc:none -gravity center  -stroke black -strokewidth 2 -annotate 0 '{TEXT}' -background none -shadow 100x3+0+0 +repage -stroke none -fill white  -annotate 0 '{TEXT}' {IMG}  +swap -gravity south -geometry +0-3 -composite  {OUTPUT}"

    # CMD = f"convert -pointsize 30 xc:none -gravity center  -stroke black -strokewidth 2 -annotate 0 '{TEXT}' -background none -shadow 100x3+0+0 +repage -resize {WIDTH}x50! -stroke none -fill white  -annotate 0 '{TEXT}' {IMG}  +swap -gravity south -geometry +0-3 -composite  {OUTPUT}"

    # CMD =f"convert -background '#0008' -fill white -gravity center -size ${WIDTH}x30   -caption '{TEXT}' {IMG} +swap -gravity south -composite  {OUTPUT}"

    POINTS = guess_points(IMG) #32
    RAT = 1
    #print(len(TEXT))
    if len(TEXT)>30:
        RAT = 30/len(TEXT)
    POINTS = round(RAT*POINTS)
    # 30 len 29
    #
    CMD = f"montage -pointsize {POINTS} -label '{TEXT}'  {IMG} -geometry +0+0 -background white {OUTPUT}"
    #convert -pointsize 100 label:"TEXT TO FIT" -trim +repage -resize 200x50! x.png

    runme(CMD)
    return OUTPUT




def dither(IMG, percent=50):
        #width = 707-10
    OUTPUT = get_tmp()
    #CMD="-auto-level  -scale "+str(width)+"x   -monochrome -dither FloydSteinberg  -remap pattern:gray50  "+OUTPUT
    CMD=f"convert {IMG} -auto-level   -monochrome -dither FloydSteinberg  -remap pattern:gray{percent}  {OUTPUT}"
    runme(CMD)
    return OUTPUT




def rotate_img(IMG):
    OUTPUT = get_tmp()
    CMD = f"convert {IMG} -rotate 90 {OUTPUT}"
    runme(CMD)
    return OUTPUT



def resize_img(IMG, factor = 0.5):
    OUTPUT = get_tmp()
    CMD = f"    convert {IMG}    -resize {round(factor*100)}%   {OUTPUT}"
    runme(CMD)
    return OUTPUT


def rescale_img(IMG, maxw = 714):
    """
    62x   brother  eats 714 px width, then it can crash
    """
    OUTPUT = get_tmp()
    CMD = f"    convert {IMG}    -resize x{maxw}   {OUTPUT}"
    runme(CMD)
    return OUTPUT



def annotate_img(IMG, north=" ", south=" "):
    """

    """
    WIDTH = get_width(IMG)
    HEIGHT = get_height(IMG)
    OUTPUTN = get_tmp()
    OUTPUTS = get_tmp()
    OUTPUT = get_tmp()

    POINTS = guess_points(IMG)

    IMN=""
    IMS=""
    if north is not None and len(north.strip())>0:
        #CMD = f"convert -background white -fill black -gravity center -size {WIDTH}x label:{north} NORTH.png"
        CMD = f"convert -background white -fill black -gravity center -pointsize {POINTS} -size {WIDTH}x{POINTS}  label:'{north}' {OUTPUTN}"
        runme(CMD)
        IMN=OUTPUTN#"NORTH.png"
    if south is not None and len(south.strip())>0:
        #CMD = f"convert -background white -fill black -gravity center -size {WIDTH}x label:{south} SOUTH.png"
        CMD = f"convert -background white -fill black -gravity center -pointsize {POINTS} -size {WIDTH}x{POINTS}  label:'{south}' {OUTPUTS}"
        runme(CMD)
        IMS=OUTPUTS#"SOUTH.png"
    CMD = f'montage -geometry +0+0 -set label "" -tile 1x {IMN} {IMG} {IMS} {OUTPUT}'
    runme(CMD)


    WIDTH = get_width(OUTPUT)
    HEIGHT = get_height(OUTPUT)
    print("i... +++++++++++++++++++++++++++++++++++++++++++++++++++++annotate" )
    print("i... HEIGHT==",HEIGHT)
    print("i... WIDTH==",WIDTH)
    print(OUTPUT)
    print("i... +++++++++++++++++++++++++++++++++++++++++++++++++++++annotate" )
    return OUTPUT



# ==============================================================================================
# ==============================================================================================
# ==============================================================================================
# ==============================================================================================
# ==============================================================================================
# ==============================================================================================
# ==============================================================================================
# ==============================================================================================
# ==============================================================================================



def main( imgname=None , nlabel=None, slabel=None , printme=False, format="62", rotate = True, xsize = None ):
    """
"14.0-usb-0:1:1.0"
    Print image on QL700 printer

    Args:
      imgname: filename to print
      nlabel:  north label
      label:  bottom-south label

       rotate:  True - means text goes left to right

    Returns:
       Bool: status code
    """
    while True:

        print("i... Call ql570 to print on brother QL700 directly. Be in lp group")
        print("i...   USE  62   - DK-22205   for printing")
        print("i...   ")
        print()

        MAIN_WIDTH_PARAMETER = 714
        height,width = 480,640
        height,width = 480,240
        height,width = 1,714
        img_work = imgname
        keypress = None
        printme = None
        myformat = str(format)


        # REDEFINE
        if imgname is None:
            imgname = "/tmp/blank.jpg"
            blank_image = np.zeros((height,width,3), np.uint8)
            blank_image[:,:] = (255,255,255)
            cv2.imwrite( imgname , blank_image)
            img_work = imgname

        if img_work is None: # returns on 'q'
            break

        nlabel = input("> Sentence : ")
        if len(nlabel.split())>1:
            slabel = nlabel.split()[0]
            nlabel = nlabel.split()[1]

        keypress = input("> Give me a command: q/p/N :")
        if keypress == "":
            keypress = "n"
        keypress = ord(keypress)
        print(keypress)

        if keypress == ord('n'):
            print("i... n returned, NOT printing")
            printme = False
        elif keypress == ord('p'):
            printme = True
            print("i... ' ' returned, printing", printme, keypress)
        elif keypress == ord('q'):
            print("i... QUIT", printme, keypress)
            break
        else:
            printme = False
            print("i... default printme = False")
        # printme = False

        #img_work = test_img(img_work)

        if xsize is not None: # some resize given in cmdline
            print(f"D... resizing to {xsize}")
            img_work = resize_img(img_work, factor=xsize)

        if nlabel is None and slabel is None:
            nlabel,slabel = get_random_labels()
        img_work = annotate_img(img_work, nlabel, slabel)


        #
        # for printing....
        #
        if rotate:
            print("D... ROTATING")
            img_work = rotate_img(img_work)


        # ===================================================== DITHER
        img_work = rescale_img(img_work, maxw = MAIN_WIDTH_PARAMETER) #
        img_work = dither(img_work, percent = 30)


        #  myformat 62 ..... FANTASTIC......
        #  29x90 ... works, but....
        #
        #
        if printme:
            CMD = f"./ql570 /dev/usb/lp0 {myformat} {img_work}"
            #CMD = f"geeqie {OUTPUT}"
            runme(CMD)
        else:
            #CMD = f"./ql570 /dev/usb/lp0 62 {OUTPUT}"
            CMD = f"geeqie {img_work}"
            runme(CMD)
    # =======================================================

    return True




if __name__=="__main__":
    Fire(main)
